package com.twuc.onlinestore.contract;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ErrorHandlerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ExistedCommodityException.class})
    public ResponseEntity handleExistedCommodityException() {
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .build();
    }
}
