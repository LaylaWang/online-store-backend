package com.twuc.onlinestore.contract;

import com.twuc.onlinestore.domain.Order;

public class GetOrderResponse {

    private String name;
    private String price;
    private int count;
    private String unit;

    public GetOrderResponse() {
    }

    public GetOrderResponse(Order order) {
        this.name = order.getCommodity().getName();
        this.price = order.getCommodity().getPrice();
        this.count = order.getCount();
        this.unit = order.getCommodity().getUnit();
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    public String getUnit() {
        return unit;
    }
}
