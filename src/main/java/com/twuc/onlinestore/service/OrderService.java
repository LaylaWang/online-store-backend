package com.twuc.onlinestore.service;

import com.twuc.onlinestore.contract.CreateOrderRequest;
import com.twuc.onlinestore.dao.OrderRepository;
import com.twuc.onlinestore.domain.Commodity;
import com.twuc.onlinestore.domain.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderService {

    private OrderRepository repository;
    private CommodityService commodityService;

    public OrderService(OrderRepository repository, CommodityService commodityService) {
        this.repository = repository;
        this.commodityService = commodityService;
    }

    public Order create(CreateOrderRequest request) {
        Commodity commodity = commodityService.getByName(request.getCommodityName());
        Order order = repository.findByCommodity(commodity).orElse(new Order(commodity, 0));
        order.addCount();
        return repository.save(order);
    }

    public List<Order> getAll() {
        return repository.findAll();
    }

    public void deleteByCommodityName(String commodityName) {
        Commodity commodity = commodityService.getByName(commodityName);
        Order order = repository.findByCommodity(commodity).get();
        repository.delete(order);
    }
}
