create table commodities (
    id int primary key auto_increment,
    name varchar(50) not null,
    price double(10, 2) not null,
    unit varchar(1) not null,
    image_url varchar(100) not null
);