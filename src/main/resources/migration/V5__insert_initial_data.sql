delete from commodities;

insert into commodities (name, price, unit, image_url)
values ('coca cola', '49.9', 'box', 'https://img14.360buyimg.com/n7/jfs/t1/17061/37/15943/229171/5cb31269E1e8695cf/a335f7a9fb189b4c.jpg'),
       ('sprite', '41.5', 'box', 'https://img12.360buyimg.com/n7/jfs/t1/69202/15/4059/131093/5d2403ecEf3206694/68dae120cc1540a9.jpg'),
       ('apple', '59.9', 'box', 'https://img11.360buyimg.com/n7/jfs/t1/17860/25/14396/380055/5ca5c895E2b6fd04f/e0be0a3fb87cbee0.jpg'),
       ('lychee', '20.6', 'pound', 'https://img12.360buyimg.com/n7/jfs/t1/36930/40/9021/147182/5cce899eE79431934/d5f4a914e114ec87.jpg'),
       ('battery', '27', 'dozen', 'https://img12.360buyimg.com/n7/jfs/t1/34660/7/2318/367544/5cb59929E555da112/2150b372b03ef384.jpg'),
       ('Instant noodles', '2.5', 'bag', 'https://img11.360buyimg.com/n9/jfs/t1/83407/2/309/412485/5ce7b260E97fc89e2/a5a491d3248c3e16.jpg')