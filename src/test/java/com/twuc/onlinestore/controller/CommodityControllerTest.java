package com.twuc.onlinestore.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.onlinestore.IntegrationTestBase;
import com.twuc.onlinestore.dao.CommodityRepository;
import com.twuc.onlinestore.domain.Commodity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CommodityControllerTest extends IntegrationTestBase {

    @Autowired
    CommodityRepository repository;

    @Test
    void should_create_commodity() throws Exception {
        Commodity cola = new Commodity("cola", "5.5", "b", "www.image.com");

        getMockMvc().perform(post("/api/commodities")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(cola)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", "http://localhost/api/commodities/1"));
    }

    @Test
    void should_status_be_400_when_post_invalid_commodity() throws Exception {
        Commodity commodity = new Commodity();

        getMockMvc().perform(post("/api/commodities")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(commodity)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_status_be_409_when_post_existed_commodity() throws Exception {
        Commodity cola = new Commodity("cola", "5.5", "b", "www.image.com");
        repository.save(cola);
        getEm().flush();
        getEm().clear();

        getMockMvc().perform(post("/api/commodities")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(cola)))
                .andExpect(status().isConflict());
    }

    @Test
    void should_get_all_commodities() throws Exception {
        Commodity cola = new Commodity("cola", "5.5", "b", "www.cola.com");
        Commodity sprite = new Commodity("sprite", "5.5", "b", "www.cola.com");
        repository.save(cola);
        repository.save(sprite);
        getEm().flush();
        getEm().clear();

        getMockMvc().perform(get("/api/commodities"))
                .andExpect(status().isOk());
    }
}
