package com.twuc.onlinestore.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.onlinestore.IntegrationTestBase;
import com.twuc.onlinestore.contract.CreateOrderRequest;
import com.twuc.onlinestore.dao.OrderRepository;
import com.twuc.onlinestore.domain.Commodity;
import com.twuc.onlinestore.domain.Order;
import com.twuc.onlinestore.service.CommodityService;
import com.twuc.onlinestore.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class OrderControllerTest extends IntegrationTestBase {

    @Autowired
    OrderController controller;
    @Autowired
    OrderService service;
    @Autowired
    OrderRepository repository;
    @Autowired
    CommodityService commodityService;

    @Test
    void should_create_order() throws Exception {
        Commodity commodity = new Commodity("cola", "5", "box", "www.image.com");
        commodityService.create(commodity);

        CreateOrderRequest request = new CreateOrderRequest(commodity.getName());
        getMockMvc().perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", "http://localhost/api/orders/1"));
    }

    @Test
    void should_create_order_2() throws Exception {
        Commodity commodity = new Commodity("cola", "5", "box", "www.image.com");
        commodityService.create(commodity);

        CreateOrderRequest request = new CreateOrderRequest(commodity.getName());
        getMockMvc().perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", "http://localhost/api/orders/1"));

        getMockMvc().perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", "http://localhost/api/orders/1"));

        Order order = repository.findByCommodity(commodity).orElse(null);
        assertEquals(2, order.getCount());
    }

    @Test
    void should_get_all_orders() throws Exception {
        Commodity commodity = new Commodity("cola", "5", "box", "www.image.com");
        commodityService.create(commodity);
        CreateOrderRequest request = new CreateOrderRequest(commodity.getName());
        service.create(request);

        getMockMvc().perform(get("/api/orders"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1));
    }

    @Test
    void should_delete_order_by_commodity_name() throws Exception {
        Commodity commodity = new Commodity("cola", "5", "box", "www.image.com");
        commodityService.create(commodity);
        CreateOrderRequest request = new CreateOrderRequest(commodity.getName());
        service.create(request);

        getMockMvc().perform(delete("/api/orders/cola"))
                .andExpect(status().isOk());
    }
}
