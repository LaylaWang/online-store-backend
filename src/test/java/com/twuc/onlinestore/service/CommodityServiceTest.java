package com.twuc.onlinestore.service;

import com.twuc.onlinestore.IntegrationTestBase;
import com.twuc.onlinestore.contract.ExistedCommodityException;
import com.twuc.onlinestore.dao.CommodityRepository;
import com.twuc.onlinestore.domain.Commodity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CommodityServiceTest extends IntegrationTestBase {

    @Autowired
    CommodityService service;
    @Autowired
    CommodityRepository repository;

    @Test
    void should_create_commodity() {
        Commodity cola = new Commodity("cola", "5.5", "b", "www.image.com");

        Commodity savedCola = service.create(cola);

        assertEquals(cola, savedCola);
    }

    @Test
    void should_throw_exception_when_create_existed_commodity() {
        Commodity cola = new Commodity("cola", "5.5", "b", "www.image.com");
        service.create(cola);

        assertThrows(ExistedCommodityException.class, () -> service.create(cola));
    }

    @Test
    void should_get_all_commodities() {
        Commodity cola = new Commodity("cola", "5.5", "b", "www.cola.com");
        Commodity sprite = new Commodity("sprite", "5.5", "b", "www.cola.com");
        repository.save(cola);
        repository.save(sprite);
        getEm().flush();
        getEm().clear();

        assertEquals(2, service.getAll().size());
    }
}
