package com.twuc.onlinestore.service;

import com.twuc.onlinestore.IntegrationTestBase;
import com.twuc.onlinestore.contract.CreateOrderRequest;
import com.twuc.onlinestore.domain.Commodity;
import com.twuc.onlinestore.domain.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceTest extends IntegrationTestBase {

    @Autowired
    OrderService service;
    @Autowired
    CommodityService commodityService;

    @Test
    void should_create_order() {
        Commodity commodity = new Commodity("cola", "5", "box", "www.image.com");
        commodityService.create(commodity);

        CreateOrderRequest request = new CreateOrderRequest("cola");
        Order savedOrder = service.create(request);

        assertEquals(commodity, savedOrder.getCommodity());
    }
}
